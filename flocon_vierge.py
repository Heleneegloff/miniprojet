#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 21:28:29 2020

@author: louisvincent
"""

from tkinter import *
from math import *
import random,numpy as np


class Position:
    """ Création de l'objet position """
    
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y


class TortueBasique:
    """ Création de l'objet Tortue """
    
    def __init__(self,coul,pos=Position()):
        self.coordonnees = pos
        self.statut = True
        self.couleur = coul
        self.epaisseur = 1
        self.angle = 0


    def aller_a_xy(self,x,y):
        """ 
        Méthode permettant de se déplacer au point de coordonnnées passées en paramètres
        :param x: int ou float, abscisse du point d'arrivée
        :param y: int ou float, ordonnée du point d'arrivée
        :return: None
        """
        
        if self.statut==True:
            zone_de_dessin.create_line(self.coordonnees.x,self.coordonnees.y,x,y,fill=self.couleur,width=self.epaisseur)
        self.coordonnees.x = x
        self.coordonnees.y = y


    def relever_stylo(self):
        """ 
        Méthode permettant de relever le stylo et ne pas tracer lors du déplacement
        :return: None
        """
        
        self.statut = False


    def abaisser_stylo(self):
        """ 
        Méthode permettant d'abaisser le stylo pour pouvoir tracer un segment
        :return: None
        """
        
        self.statut = True


    def changer_couleur(self,couleur):
        """ 
        Méthode permettant de modifier la couleur du stylo
        :return: None
        """

        self.couleur = couleur


    def changer_epaisseur(self,val):
        """ 
        Méthode permettant de modifier l'épaisseur du stylo
        :return: None
        """
        
        self.epaisseur = val


    def tourner_gauche(self,angle_deg):
        """ 
        Méthode permettant d'orienter le tracer vers la gauche suivant un angle
        :param angle_deg : int ou float, angle en degrés de la rotation
        :return : None
        """
        
        self.angle -= angle_deg/180*pi


    def tourner_droite(self,angle_deg):
        """ 
        Méthode permettant d'orienter le tracer vers la droite suivant un angle
        :param angle_deg: int ou float, angle en degrés de la rotation
        ;return  None
        """
        
        self.angle += angle_deg/180*pi


    def avancer (self,longueur):
        """ 
        Méthode permettant d'avancer d'une longueur fixée suivant un angle fixé
        :param longueur: int ou float, longueur en pixels du déplacement
        :return :
        """
        
        x = self.coordonnees.x+longueur*cos(self.angle)
        y = self.coordonnees.y+longueur*sin(self.angle)
        
        self.aller_a_xy(x,y)


    def koch(self,n,l):
        if n == 0:
            self.abaisser_stylo()
            self.avancer(l)
        else:
            self.koch(n-1, l/3)
            self.tourner_gauche(60)
            self.koch(n-1, l/3)
            self.tourner_droite(120)
            self.koch(n-1, l/3)
            self.tourner_gauche(60)
            self.koch(n-1, l/3)

    def etoileKoch(self,n,l):
        self.tourner_droite(60)
        self.koch(n,l)
        self.tourner_droite(120)
        self.koch(n,l)
        self.tourner_droite(120)
        self.koch(n,l)




    def triangle_sierpinski(self,n,l):
        if n != 0:
            for _ in range(3):
                self.triangle_sierpinski(n-1, l/2)
                self.avancer(l)
                self.tourner_gauche(120)


    def arbre_bin(self,n,l):
        if n != 1:

            #sous arbre droit
            x,y = self.coordonnees.x,self.coordonnees.y
            self.tourner_droite(60)
            self.avancer(l)
            self.angle=0
            self.arbre_bin(n-1, l/2)

            #on remet les coordonées à la racine
            self.coordonnees.x,self.coordonnees.y = x,y

            #sous arbre gauche
            self.tourner_droite(120)
            self.avancer(l)
            self.angle=0
            self.arbre_bin(n-1, l/2)

    def tete(self,x,y,r):
        cosS = [cos(x)*r for x in range(100)]
        sinS = [sin(x)*r for x in range(100)]

        self.relever_stylo()
        self.aller_a_xy(x+r, y)
        self.abaisser_stylo()

        for i in range(len(cosS)):
            self.aller_a_xy(cosS[i]+x, sinS[i]+y)

    def cheveux(self,x,y , longueur):
        """"
        ici on a des cheveux !!!
        """
        cosS = [cos(0.1*x + 20)*10 for x in range(longueur)]
        self.relever_stylo()
        self.aller_a_xy(x, y)
        self.abaisser_stylo()
        for xx in range(longueur):
            self.aller_a_xy( cosS[xx]+x, xx+y)





def neige(tortue, x=0):    
    zone_de_dessin.delete(ALL)
    draw()
    tortue.relever_stylo()
    tortue.aller_a_xy(x, tortue.coordonnees.y)
    tortue.relever_stylo()
    tortue.etoileKoch(3, 50)
    tortue.angle=0
    if x > 800:
        tortue.relever_stylo()
        tortue.aller_a_xy(140,400)
        tortue.abaisser_stylo()
    fenetre.after(1,neige,tortue,tortue.coordonnees.x+10)
# =============================================================================
# PROGRAMME PRINCIPAL
# =============================================================================


fenetre = Tk()
zone_de_dessin = Canvas(width=1000, height=600, background="white")
zone_de_dessin.pack()

# ### Tâche 1 : segment à découper
# # Création de l’objet tortue1 en donnant au stylo la couleur verte
""" tortue1 = TortueBasique("green")
tortue1.changer_epaisseur(3)
tortue1.relever_stylo()
tortue1.aller_a_xy(200,200)
tortue1.abaisser_stylo()
tortue1.koch(6,400) """

# ### Tâche 2 : flocon de Koch
# # Suppression du dessin précédent
# # Création de l’objet tortue2 en donnant au stylo la couleur bleue
# ici c'est une animation de helène qui m'envoie une boule de neige 

#tortue = TortueBasique("black")
#tortue.aller_a_xy(random.randint(100, 900), 0)
#fenetre.after(30,neige,tortue)

def draw():
    tortueHelene = TortueBasique("black")

    tortueHelene.relever_stylo()
    tortueHelene.aller_a_xy(140, 400)
    tortueHelene.tourner_droite(90)

    tortueHelene.relever_stylo()
    tortueHelene.avancer(40)
    tortueHelene.tourner_gauche(120)
    tortueHelene.abaisser_stylo()
    tortueHelene.avancer(50)
    tortueHelene.relever_stylo()
    tortueHelene.aller_a_xy(140, 400)
    tortueHelene.angle=0
    tortueHelene.tourner_droite(90)

    tortueHelene.abaisser_stylo()
    tortueHelene.avancer(100)
    tortueHelene.angle = 0
    tortueHelene.arbre_bin(2, 100)
    tortueHelene.relever_stylo()
    tortueHelene.tete(140,370,30)
    tortueHelene.tete(130,370,5)
    tortueHelene.tete(160,370,5)
    tortueHelene.couleur = "brown"
    tortueHelene.epaisseur = 5
    tortueHelene.cheveux(120, 350, 70)
    tortueHelene.cheveux(130, 345 , 70)
    tortueHelene.cheveux(160, 350, 70)
    tortueHelene.cheveux(150, 350, 70)

    tortueJibril = TortueBasique("black")
    tortueJibril.relever_stylo()
    tortueJibril.aller_a_xy(800, 400)
    tortueJibril.tourner_droite(90)
    tortueJibril.abaisser_stylo()
    tortueJibril.avancer(100)
    tortueJibril.angle = 0
    tortueJibril.arbre_bin(2, 100)
    tortueJibril.relever_stylo()
    tortueJibril.tete(800,370,30)
    tortueJibril.tete(780,370,5)
    tortueJibril.tete(810,370,5)
    tortueJibril.couleur = "black"
    tortueJibril.epaisseur = 5
    tortueJibril.cheveux(770+10, 350, 30)
    tortueJibril.cheveux(780+10, 340 , 30)
    tortueJibril.cheveux(790+10, 340, 30)
    tortueJibril.cheveux(800+10, 350, 30)


tortueFlocon = TortueBasique("black")
neige(tortueFlocon)


# ### Tâche 3 : triangle de Sierpinski
# # Suppression du dessin précédent
""" zone_de_dessin.delete(ALL)

# # Création de l’objet tortue2 en donnant au stylo la couleur rouge
tortue3 = TortueBasique("red")
tortue3.changer_epaisseur(1) 
tortue3.relever_stylo()
tortue3.aller_a_xy(200,550)
tortue3.abaisser_stylo()
tortue3.triangle_sierpinski(10,600) """



# ### Tâche 4 : Arbre binaire
# # Suppression du dessin précédent

# # Création de l’objet tortue2 en donnant au stylo la couleur orange
""" tortue4 = TortueBasique("orange") 
tortue4.changer_epaisseur(3) 
tortue4.relever_stylo()
tortue4.aller_a_xy(500,15)
tortue4.abaisser_stylo()
tortue4.arbre_bin(5,300) """


# je vais essayer de faire de la 3D
# moteur de rendu avec du ray casting




fenetre.mainloop() # Ne pas enlever !!! Ni rien placer au delà ...
